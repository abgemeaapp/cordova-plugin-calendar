module.exports = {
  createEventWithOptions: function(win, fail, args) {
    console.log(args);
    var appointment = new Windows.ApplicationModel.Appointments.Appointment();
    appointment.startTime = new Date(2014, 2, 28, 18); // March 28th, 2014 at 6:00pm
    appointment.duration = (60 * 60 * 100000) / 100; // 1 hour in 100ms units
    appointment.location = "Ben Miller's home";
    appointment.subject = "Frank's Birthday";
    appointment.details = "Surprise party to celebrate Frank's 60th birthday! Hoping you all can join us.";
    appointment.reminder = (15 * 60 * 1000000000) / 100; // Remind me 15 minutes prior to appointment

    // Get the selection rect of the button pressed to add this appointment
    //var boundingRect = e.srcElement.getBoundingClientRect();
    var selectionRect = {
      x: 0,
      y: 0,
      width: 300,
      height: 500
    };

    // ShowAddAppointmentAsync returns an appointment id if the appointment given was added to the user's calendar.
    // This value should be stored in app data and roamed so that the appointment can be replaced or removed in the future.
    // An empty string return value indicates that the user canceled the operation before the appointment was added.
    Windows.ApplicationModel.Appointments.AppointmentManager.showAddAppointmentAsync(
        appointment, selectionRect, Windows.UI.Popups.Placement.default)
      .done(function(appointmentId) {
        if (appointmentId) {
          document.querySelector('#result').innerText =
            "Appointment Id: " + appointmentId;
        } else {
          document.querySelector('#result').innerText = "Appointment not added";
        }
      });
  }
}
